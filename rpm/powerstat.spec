Name:    powerstat
Summary: System service for managing device power configuration
Version: 0.1.0
Release: 1
License: Apache 2.0
Group:   System/Daemons
Source0: %{name}-%{version}.tar.zst
BuildRequires: meson
BuildRequires: sdbus-cpp-devel
BuildRequires: pkgconfig(sqlite3)
Requires:      sdbus-cpp

%description
This package contains the Aurora OS power stat daemon.

%package devel
Summary: Development files for powerstat
Requires: %{name} = %{version}-%{release}

%description devel
C++ library wrapping D-Bus calss to powerstat service.

%prep
%autosetup

%build
%meson
%meson_build

%install
%meson_install

%post
/sbin/ldconfig
mkdir /usr/share/powerstat
systemctl daemon-reload > /dev/null 2>&1 || :
systemctl reload-or-try-restart %{name}.service > /dev/null 2>&1 || :

%preun
if [ "$1" -eq 0 ]; then
    # Package removal, not upgrade
    systemctl stop %{name}.service > /dev/null 2>&1 || :
fi

%postun -p /sbin/ldconfig

%files
%{_sbindir}/%{name}
%{_unitdir}/%{name}.service
%{_libdir}/lib%{name}.so.*
%{_sysconfdir}/dbus-1/system.d/ru.omp.PowerStat.conf

%files devel
%{_includedir}/%{name}.h
%{_libdir}/lib%{name}.so
%{_libdir}/pkgconfig/%{name}.pc
