# PowerStat Service

D-Bus Service interface class was generated via `sdbus-c++-xml2cpp` utility.

```bash
sdbus-c++-xml2cpp dbus/powerstat.xml --adaptor=src/powerstat_interface.h
```