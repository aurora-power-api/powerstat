/*
 * Copyright 2023 Dmitriy Butakov
 */

#pragma once

#include <map>

namespace Aurora {
namespace Power {
class PowerStat
{
public:
    static uint64_t lastFullChargedTime();
    static uint64_t battryTimeToDischarge();
    static std::map<uint64_t, uint16_t> batteryDischargeHistory();
};
} // namespace Power
} // namespace Aurora
