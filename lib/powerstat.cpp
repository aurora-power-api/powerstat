/*
 * Copyright 2023 Dmitriy Butakov
 */

#include "powerstat.h"
#include "powerstat_client.h"

#include <iostream>

namespace Aurora {
namespace Power {
namespace {
const char *destinationName = "ru.omp.PowerStat";
const char *objectPath = "/ru/omp/PowerStat";
} // namespace

uint64_t PowerStat::lastFullChargedTime()
{
    PowerStatClient dbusClient(destinationName, objectPath);
    return dbusClient.getLastFullChargedTime();
}

uint64_t PowerStat::battryTimeToDischarge()
{
    PowerStatClient dbusClient(destinationName, objectPath);
    return dbusClient.getBattryTimeToDischarge();
}

std::map<uint64_t, uint16_t> PowerStat::batteryDischargeHistory()
{
    PowerStatClient dbusClient(destinationName, objectPath);
    std::vector<sdbus::Struct<uint64_t, uint16_t>> result = dbusClient.getBatteryDischargeHistory();
    std::map<uint64_t, uint16_t> stdResult;
    for (const auto &tupledStruct: result) {
        stdResult[std::get<uint64_t>(tupledStruct)] = std::get<uint16_t>(tupledStruct);
    }
    return stdResult;
}

} // namespace Power
} // namespace Aurora