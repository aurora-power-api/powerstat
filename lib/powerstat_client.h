/*
 * Copyright 2023 Dmitriy Butakov
 */

#pragma once

#include "powerstat_proxy.h"

#include <sdbus-c++/sdbus-c++.h>

class PowerStatClient : public sdbus::ProxyInterfaces<ru::omp::PowerStat_proxy>
{
public:
    PowerStatClient(std::string destination, std::string objectPath)
        : ProxyInterfaces(std::move(destination), std::move(objectPath))
    {
        registerProxy();
    }

    ~PowerStatClient() { unregisterProxy(); }
};
