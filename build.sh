#!/bin/bash

usage() {
    cat <<EOF

Build project in CI

Usage:
$(basename $0) [OPTION]

Options:
-n   | --name    <STRING>                project name
-v   | --version <STRING>                Aurora Platform SDK version
-r   | --release <STRING>                Aurora Platform SDK release
-t   | --target  [i486|armv7hl|aarch64]  select target
-h   | --help                            this help

EOF

    # exit if any argument is given
    [[ -n "$1" ]] && exit 1
}

fail() {
    echo ""
    echo "FAIL: $@"
    usage
    exit 1
}

# handle commandline options
while [[ ${1:-} ]]; do
    case "$1" in
    -n | --name ) shift
        OPT_PROJECT_NAME=$1; shift
        ;;
    -v | --version ) shift
        OPT_PSDK_VERSION=$1; shift
        ;;
    -r | --release ) shift
        OPT_PSDK_DEVEL_RELEASE=$1; shift
        ;;
    -t | --target ) shift
        OPT_TARGET=$1; shift
        ;;
    -h | --help ) shift
        usage quit
        ;;
    * )
        usage quit
        ;;
    esac
done

if [[ -z $OPT_PROJECT_NAME ]]; then
    fail "enter project name"
fi

if [[ -z $OPT_PSDK_VERSION ]] || [[ -z $OPT_PSDK_DEVEL_RELEASE ]]; then
    fail "incorrect version or release"
fi

if [[ "$OPT_TARGET" != "i486" ]] && [[ "$OPT_TARGET" != "armv7hl" ]] && [[ "$OPT_TARGET" != "aarch64" ]]; then
    fail "incorrect target"
fi

PROJECT_NAME=$OPT_PROJECT_NAME
PSDK_VERSION=$OPT_PSDK_VERSION
PSDK_DEVEL_RELEASE=$OPT_PSDK_DEVEL_RELEASE

PROJECT_DIR="./$PROJECT_NAME"
CI_BUILD_DIR=$PWD

PSDK_FULL_VERSION=$PSDK_VERSION.$PSDK_DEVEL_RELEASE
PSDK_TARGET=Aurora-$PSDK_FULL_VERSION

TARGET=$PSDK_TARGET-$OPT_TARGET

# Build project
echo "Build $PROJECT_NAME project for $TARGET"
mb2 --target $TARGET build

# Copy build artifacts
mkdir -p $CI_BUILD_DIR/artifacts
cp RPMS/* $CI_BUILD_DIR/artifacts/
