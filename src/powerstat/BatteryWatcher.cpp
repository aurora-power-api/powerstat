/*
 * Copyright 2023 Dmitriy Butakov
 */

#include "BatteryWatcher.hpp"
#include "../common/DbHandler.hpp"
#include "../common/Log.hpp"

#include <chrono>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

namespace Aurora {
namespace Private {
namespace {
const char *BATTERY_LEVEL_PATH = "/sys/class/power_supply/battery/capacity";
const char *BATTERY_FULL_CAPACITY = "/sys/class/power_supply/battery/charge_full";
const char *BATTERY_VOLTAGE_PATH = "/sys/class/power_supply/battery/voltage_now";
const char *BATTERY_CURRENT_PATH = "/sys/class/power_supply/battery/current_now";
const char *BATTERY_HISTORY_FILE = "/usr/share/powerstat/battery_history.db";
const char *BATTERY_HISTORY_TABLE = "BATTERY_HISTORY";

inline uint64_t currentTimeMs()
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(
               std::chrono::system_clock::now().time_since_epoch())
        .count();
}

std::string getFirstLine(const std::string &path)
{
    std::string line; 
    std::ifstream file(path);
    if (!file) {
        LOG_ERROR("Failed to open: " << path);
        return line;
    }

    std::getline(file, line);
    file.close();
    return line;
}

int checkAndUpdateHistory() {
    std::string levelStr = getFirstLine(BATTERY_LEVEL_PATH);
    if (levelStr.empty()) {
        return -1;
    }

    uint16_t level = std::stoul(levelStr);
    DbHandler batteryHistory(BATTERY_HISTORY_FILE, BATTERY_HISTORY_TABLE);
    if (level == batteryHistory.getLastValue()) {
        LOG_WARN("Captured level is the same");
        return 0;
    }

    int res = batteryHistory.insertData(currentTimeMs(), level);
    if (res < 0) {
        LOG_ERROR("Failed to update history");
        return -1;
    }

    return 0;
}
} // namespace

BatteryWatcher::BatteryWatcher()
    : m_lastFullChargeTime(0)
    , m_batteryTimeToDischarge(0)
{
    // Make sure, that history exisists
    if (checkAndUpdateHistory() < 0) {
        LOG_ERROR("Failed to process battery history.");
        return;
    }
    /* Important! Batttery watcher should be registred right after
     * history check, to avoid recursion on catching access event
     */
    m_batteryWatcher.reset(new Watcher(BATTERY_LEVEL_PATH, Watcher::Type::File));

    DbHandler batteryHistory(BATTERY_HISTORY_FILE, BATTERY_HISTORY_TABLE);
    if (batteryHistory.getLastMaxValue() != 0)
        LOG_WARN("Device was never fully charged!");

    m_batteryWatcher->registerCallback(&BatteryWatcher::updateHistory);
}

uint64_t BatteryWatcher::getLastFullChargedTime() const
{
    return m_lastFullChargeTime;
}

uint64_t BatteryWatcher::getBattryTimeToDischarge() const
{
    /* Basic approach, ensure with help of calculating app consmption
     * t = P / C = U * I / C
     * where t is time left, P is power, C is capacity now, U is voltage now,
     * I is current now
     */
    uint64_t voltageNow = std::atoi(getFirstLine(BATTERY_VOLTAGE_PATH).c_str());
    uint64_t currentNow = -std::atoi(getFirstLine(BATTERY_CURRENT_PATH).c_str());
    uint64_t levelNow = std::atoi(getFirstLine(BATTERY_LEVEL_PATH).c_str()); // get percentage
    uint64_t fullCapacity = std::atoi(getFirstLine(BATTERY_FULL_CAPACITY).c_str()); // get full
    uint64_t capacityNow = fullCapacity / 100 * levelNow;

    return voltageNow * currentNow / capacityNow;
}

std::map<uint64_t, uint16_t> BatteryWatcher::getBatteryDischargeHistory() const
{
    DbHandler batteryHistory(BATTERY_HISTORY_FILE, BATTERY_HISTORY_TABLE);
    return batteryHistory.getData();
}

void BatteryWatcher::updateHistory(const std::string &content, int events)
{
    UNUSED(events); // Doesn't matter here

    uint64_t now = currentTimeMs();

    DbHandler batteryHistory(BATTERY_HISTORY_FILE, BATTERY_HISTORY_TABLE);
    uint16_t lastValue = batteryHistory.getLastValue();
    uint16_t newValue = std::strtol(content.c_str(), nullptr, 10);
    if (lastValue == newValue)
        return; // Ignoring

    batteryHistory.insertData(now, newValue);
}
} // namespace Private
} // namespace Aurora
