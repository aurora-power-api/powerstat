/*
 * Copyright 2023 Dmitriy Butakov
 */
#include "PowerStat.hpp"


int main()
{
    const char *serviceName = "ru.omp.PowerStat";
    auto connection = sdbus::createSystemBusConnection(serviceName);

    const char *objectPath = "/ru/omp/PowerStat";
    Aurora::Private::PowerStat service(*connection, objectPath);

    connection->enterEventLoop();
    return 0;
}
