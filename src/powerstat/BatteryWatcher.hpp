/*
 * Copyright 2023 Dmitriy Butakov
 */

#pragma once

#include "../common/Watcher.hpp"

#include <cstdint>
#include <string>
#include <map>

namespace Aurora {
namespace Private {
class BatteryWatcher
{
public:
    explicit BatteryWatcher();
    ~BatteryWatcher() = default;

    uint64_t getLastFullChargedTime() const;
    uint64_t getBattryTimeToDischarge() const;
    std::map<uint64_t, uint16_t> getBatteryDischargeHistory() const;

    static void updateHistory(const std::string &, int);

private:
    uint64_t m_lastFullChargeTime;
    uint64_t m_batteryTimeToDischarge;

    std::unique_ptr<Watcher> m_batteryWatcher;
};
} // namespace Private
} // namespace Aurora
