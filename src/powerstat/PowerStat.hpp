/*
 * Copyright 2023 Dmitriy Butakov
 */

#include "BatteryWatcher.hpp"
#include "PowerStat_adaptor.hpp"

#include <sdbus-c++/sdbus-c++.h>

namespace Aurora {
namespace Private {
class PowerStat : public sdbus::AdaptorInterfaces<ru::omp::PowerStat_adaptor>
{
public:
    PowerStat(sdbus::IConnection &connection, std::string objectPath);
    virtual ~PowerStat();

protected:
    uint64_t getLastFullChargedTime() override;
    uint64_t getBattryTimeToDischarge() override;
    std::vector<sdbus::Struct<uint64_t, uint16_t>> getBatteryDischargeHistory() override;

private:
    BatteryWatcher m_batteryWatcher;
};

} // namespace Private
} // namespace Aurora
