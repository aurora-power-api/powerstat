/*
 * Copyright 2023 Dmitriy Butakov
 */

#include "PowerStat.hpp"

namespace Aurora {
namespace Private {
PowerStat::PowerStat(sdbus::IConnection &connection, std::string objectPath)
    : AdaptorInterfaces(connection, std::move(objectPath))
{
    registerAdaptor();
}

PowerStat::~PowerStat()
{
    unregisterAdaptor();
}

uint64_t PowerStat::getLastFullChargedTime()
{
    return m_batteryWatcher.getLastFullChargedTime();
}

uint64_t PowerStat::getBattryTimeToDischarge()
{
    return m_batteryWatcher.getBattryTimeToDischarge();
}

std::vector<sdbus::Struct<uint64_t, uint16_t>> PowerStat::getBatteryDischargeHistory()
{
    std::vector<sdbus::Struct<uint64_t, uint16_t>> batteryHistory;
    for (const auto &[time, level] : m_batteryWatcher.getBatteryDischargeHistory()) {
        batteryHistory.push_back({time, level});
    }
    return batteryHistory;
}
} // namespace Private
} // namespace  Aurora
