/*
 * Copyright 2023 Dmitriy Butakov
 */

#include "DbHandler.hpp"
#include "Log.hpp"

#include <sqlite3.h>

namespace Aurora {
namespace Private {
DbHandler::DbHandler(const std::string &dbPath, const std::string &tableName)
    : m_db(nullptr)
    , m_dbPath(dbPath)
    , m_dbStatus(Status::Error)
{
    int rc = sqlite3_open(dbPath.c_str(), &m_db);
    if (rc != SQLITE_OK) {
        LOG_ERROR("Can't open database " << dbPath << ": " << sqlite3_errmsg(m_db));
        sqlite3_close(m_db);
        return;
    }

    char *errMsg = nullptr;
    std::string sql = "CREATE TABLE if not exists " + tableName + "("
                      "TIME STRING PRIMARY KEY NOT NULL,"
                      "VALUE INT NOT NULL);";

    rc = sqlite3_exec(m_db, sql.c_str(), nullptr, 0, &errMsg);
    if (rc != SQLITE_OK) {
        LOG_ERROR("SQL error: " << errMsg);
        sqlite3_close(m_db);
        sqlite3_free(errMsg);
        return;
    }

    m_tableName = tableName;
    m_dbStatus = Status::Ok;
}

DbHandler::~DbHandler()
{
    if (m_db)
        sqlite3_close(m_db);
}

std::map<uint64_t, uint16_t> DbHandler::getData()
{
    std::map<uint64_t, uint16_t> result;
    if (m_dbStatus == Status::Error) {
        LOG_ERROR(m_dbPath << " not initiliezed.");
        return result;
    }

    std::string sql = "SELECT TIME, VALUE from " + m_tableName  + ";";
    sqlite3_stmt *stmt = nullptr;
    int rc = sqlite3_prepare_v2(m_db, sql.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("while compiling sql: " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        return result;
    }

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        // We know, that there are 2 columns for sure
        std::string timeStr = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
        std::string value = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1));
        uint64_t time = std::stoull(timeStr);
        result[time] = std::stoul(value);
    }

    if (rc != SQLITE_DONE) {
        LOG_ERROR("select statement didn't finish with DONE" << sqlite3_errmsg(m_db));
    }

    sqlite3_finalize(stmt);

    return result;
}

int DbHandler::insertData(uint64_t key, uint16_t value)
{
    // TODO: use sqlite DEFAULT TIMESTAMP https://www.sqlite.org/lang_createtable.html#the_default_clause
    // instead of calculating it through C++ code
    if (m_dbStatus == Status::Error) {
        LOG_ERROR(m_dbPath << " not initiliezed.");
        return -1;
    }

    std::string sql = "INSERT INTO " + m_tableName + " (TIME,VALUE) VALUES (?,?)";
    sqlite3_stmt *stmt = nullptr;
    int rc = sqlite3_prepare_v2(m_db, sql.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("while compiling sql: " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        return -1;
    }

    std::string keyStr = std::to_string(key);
    rc = sqlite3_bind_text(stmt, 1, keyStr.c_str(), keyStr.length(), nullptr);
	if (rc != SQLITE_OK) {
		LOG_ERROR("error binding 1st value in insert " << rc << ": " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
		sqlite3_close(m_db);
		return -1;
    }

    std::string valueStr = std::to_string(value);
    rc = sqlite3_bind_text(stmt, 2, valueStr.c_str(), valueStr.length(), nullptr);
	if (rc != SQLITE_OK) {
		LOG_ERROR("error binding 2nd value in insert " << rc << ": " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
		sqlite3_close(m_db);
		return -1;
    }

    rc = sqlite3_step(stmt);
    sqlite3_finalize(stmt);

	if (rc != SQLITE_DONE) {
		LOG_ERROR("insert statement didn't return DONE: " << rc << ": " << sqlite3_errmsg(m_db));
        return -1;
	}

    return 0;
}

uint16_t DbHandler::getLastValue()
{
    if (m_dbStatus == Status::Error) {
        LOG_ERROR(m_dbPath << " not initiliezed.");
        return UINT16_MAX;
    }

    std::string sql = "SELECT VALUE FROM " + m_tableName + " ORDER BY TIME DESC LIMIT 1;";
    sqlite3_stmt *stmt = nullptr;
    int rc = sqlite3_prepare_v2(m_db, sql.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("while compiling sql: " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        return UINT16_MAX;
    }
    rc = sqlite3_step(stmt);

	if (rc != SQLITE_DONE && rc != SQLITE_ROW) {
		LOG_ERROR("select statement didn't return DONE: " << rc << ": " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        return UINT16_MAX;
	}

    if (sqlite3_column_type(stmt, 0) == SQLITE_NULL) {
        LOG_WARN("last value not found");
        sqlite3_finalize(stmt);
        return UINT16_MAX;
    }

    std::string value = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
    sqlite3_finalize(stmt);

    return std::strtoul(value.c_str(), nullptr, 10);
}

uint16_t DbHandler::getLastMaxValue()
{
    if (m_dbStatus == Status::Error) {
        LOG_ERROR(m_dbPath << " not initiliezed.");
        return UINT16_MAX;
    }

    std::string sql = "SELECT MAX(VALUE) FROM " + m_tableName + " ORDER BY TIME DESC LIMIT 1;";
    sqlite3_stmt *stmt = nullptr;
    int rc = sqlite3_prepare_v2(m_db, sql.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("while compiling sql: " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        return UINT16_MAX;
    }

    rc = sqlite3_step(stmt);

	if (rc != SQLITE_DONE && rc != SQLITE_ROW) {
		LOG_ERROR("select statement didn't return DONE: " << rc << ": " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        return UINT16_MAX;
	}

    if (sqlite3_column_type(stmt, 0) == SQLITE_NULL) {
        LOG_DEBUG("max value not found");
        sqlite3_finalize(stmt);
        return UINT16_MAX;
    }

    std::string value = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
    sqlite3_finalize(stmt);

    return std::strtoul(value.c_str(), nullptr, 10);
}
} // namespace Private
} // namespace Aurora