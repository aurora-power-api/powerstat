/*
 * Copyright 2023 Dmitriy Butakov
 */

#pragma once

#include <functional>
#include <future>
#include <string>

namespace Aurora {
namespace Private {
class Watcher
{
public:
    enum class Type {
        File,
        Dir
    };

    explicit Watcher(const std::string &path, Type watchedEntotyType);
    virtual ~Watcher();

    void registerCallback(std::function<void(const std::string &, int)>);

private:
    void watchFile();
    void watchDir();

    std::string m_watchedPath;
    std::function<void(const std::string &, int)> m_callback;
    std::future<void> m_watcher;

    int m_inotifyFd;
    int m_watchFd;
    bool m_stopWatching;
    Type m_entityType;
};
} // namespace Private
} // namespace Aurora