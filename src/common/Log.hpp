/*
 * Copyright 2023 Dmitriy Butakov
*/

#pragma once

#include <iostream>
#include <string>

#ifndef DEBUG
#define LOG_DEBUG(message)
#else
#define LOG_DEBUG(message) \
    std::cout << "DEBUG: " << __PRETTY_FUNCTION__ << ": " << message << std::endl;
#endif

#define LOG_WARN(message) \
    std::cout << "WARNING: " << __PRETTY_FUNCTION__ << ": " << message << std::endl;
#define LOG_ERROR(message) \
    std::cerr << "ERROR: " << __PRETTY_FUNCTION__ << ": " << message << std::endl;

#define UNUSED(x) (void) x