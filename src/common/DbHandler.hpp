/*
 * Copyright 2023 Dmitriy Butakov
 */

#pragma once

#include <map>
#include <memory>
#include <string>

#include <sqlite3.h>

#include "Log.hpp"

namespace Aurora {
namespace Private {
class DbHandler
{
public:
    explicit DbHandler(const std::string &dbPath, const std::string &tableName);
    virtual ~DbHandler();

    std::map<uint64_t, uint16_t> getData();
    int insertData(uint64_t key, uint16_t value);
    uint16_t getLastValue();
    uint16_t getLastMaxValue();

private:
    enum class Status { Ok, Error };

    sqlite3 *m_db;
    std::string m_dbPath;
    std::string m_tableName;
    Status m_dbStatus;
};
} // namespace Private
} // namespace Aurora
