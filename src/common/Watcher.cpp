/*
 * Copyright 2023 Dmitriy Butakov
 */

#include "Watcher.hpp"
#include "Log.hpp"

#include <cerrno>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>

#include <linux/limits.h>
#include <poll.h>
#include <sys/inotify.h>
#include <sys/syscall.h>
#include <unistd.h>

namespace Aurora {
namespace Private {
namespace {
const size_t EVENT_SIZE = sizeof(inotify_event);
const uint16_t EVENT_BUF_SIZE = PATH_MAX;

std::string readFile(const std::string &path)
{
    std::ifstream file(path);
    if (!file) {
        LOG_ERROR("Failed to open file: " << path);
        return {};
    }

    std::stringstream buffer;
    buffer << file.rdbuf();
    file.close();

    std::string result = buffer.str();
    result.pop_back(); // Remove EOF symbol, looks ugly, better approach?
    return result;
}
} // namespace

Watcher::Watcher(const std::string &path, Type watchedEntityType)
    : m_callback(nullptr)
    , m_stopWatching(false)
    , m_entityType(watchedEntityType)
{
    m_inotifyFd = inotify_init();
    if (m_inotifyFd < 0) {
        LOG_ERROR("inotify_init() failed: " << std::strerror(errno));
        return;
    }

    m_watchFd = inotify_add_watch(m_inotifyFd, path.c_str(), IN_ACCESS);
    if (m_watchFd < 0) {
        LOG_ERROR("inotify_add_watch() failed for " << path << ": " << std::strerror(errno));
        return;
    }
    m_watchedPath = path;
}

Watcher::~Watcher()
{
    m_stopWatching = true;
    m_watcher.wait();
}

void Watcher::registerCallback(std::function<void(const std::string &, int)> callback)
{
    if (!callback) {
        LOG_ERROR("No callback function was provided!");
        return;
    }

    m_callback = callback;
    if (m_entityType == Type::File) {
        m_watcher = std::async(std::launch::async, &Watcher::watchFile, this);
    } else {
        m_watcher = std::async(std::launch::async, &Watcher::watchDir, this);
    }
}

void Watcher::watchFile()
{
    char buffer[EVENT_BUF_SIZE];
    size_t length;

    struct pollfd inotify_fd = {m_inotifyFd, POLLIN, 0};

    while (!m_stopWatching) {
        if (poll(&inotify_fd, 1, -1) < 0) {
            LOG_ERROR("poll() failed" << std::strerror(errno));
            return;
        }
    
        /*
        * The following solution is a workaround, since some files have only
        * read permissions, they're may be provided by Linux kerenl, thus
        * writing into these files occur without syscall, and therefore instead of
        * handling IN_MODIFY, we handle IN_ACCESS.
        * 
        * Beacase callback accesses file as well, we potentially fall into recursion,
        * where each callback triggers the inotify event, thus, we remove inotify 
        * watcher right before callback execution, and return it back, when callback
        * finished.
        */

        if (inotify_rm_watch(m_inotifyFd, m_watchFd) < 0) {
            LOG_ERROR("Failed to delete inotify watcher! Quiting to avoid recursion.");
            break;
        }
        LOG_DEBUG("Watcher removed for " << m_watchedPath)

        if ((inotify_fd.revents & POLLIN)
            && (length = read(m_inotifyFd, buffer, EVENT_BUF_SIZE)) > 0) {
            memset(buffer, 0, EVENT_BUF_SIZE);
            std::string fileContents = readFile(m_watchedPath);
            m_callback(fileContents, inotify_fd.revents);
        }

        m_watchFd = inotify_add_watch(m_inotifyFd, m_watchedPath.c_str(), IN_ACCESS);
        if (m_watchFd < 0) {
            LOG_ERROR("Failed to re-add watch!");
            break;
        }
        LOG_DEBUG("Watcher added for " << m_watchedPath)
    }
}

void Watcher::watchDir()
{
    char buffer[EVENT_BUF_SIZE];
    size_t length;

    struct pollfd inotify_fd = {m_inotifyFd, POLLIN, 0};

    while (!m_stopWatching) {
        if (poll(&inotify_fd, 1, -1) < 0) {
            LOG_ERROR("poll() failed" << std::strerror(errno));
            return;
        }

        if ((inotify_fd.revents & POLLIN)
            && (length = read(m_inotifyFd, buffer, EVENT_BUF_SIZE)) > 0) {
            m_callback(buffer, inotify_fd.revents);
            memset(buffer, 0, EVENT_BUF_SIZE);
        }
    }
}

} // namespace Private
} // namespace Aurora