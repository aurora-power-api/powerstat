/*
 * Copyright 2023 Dmitriy Butakov
 */

#include "AppWatcher.hpp"
#include "AppDbHandler.hpp"
#include "../common/Log.hpp"

#include <chrono>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <limits.h>
#include <sys/inotify.h>
#include <unistd.h>

namespace Aurora {
namespace Private {
namespace {
const char *PROC_PATH = "/proc/";
const char *APP_HISTORY_PATH = "/usr/share/appstat/app_history.db";
const char *APP_HISTORY_TABLE = "APP_HISTORY";

inline uint64_t currentTimeMs()
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(
               std::chrono::system_clock::now().time_since_epoch())
        .count();
}

std::string getAppFullPath(pid_t pid) {
    std::string path = PROC_PATH + std::to_string(pid) + "/exe";
    char buf[PATH_MAX];
    int nbytes = readlink(path.c_str(), buf, PATH_MAX);
    if (nbytes == -1) {
        LOG_ERROR("readlink failed");
        return {};
    }
    return std::string(buf);
}
} // namespace

AppWatcher::AppWatcher()
{
    AppDbHandler appHistory(APP_HISTORY_PATH, APP_HISTORY_TABLE);
    m_procWatcher.reset(new Watcher(PROC_PATH, Watcher::Type::Dir));
    m_procWatcher->registerCallback(&AppWatcher::updateHistory);
}

std::map<std::string, std::map<uint64_t, uint64_t>> AppWatcher::getAppActivity() const
{
    AppDbHandler appHistory(APP_HISTORY_PATH, APP_HISTORY_TABLE);
    return appHistory.getData();
}

void AppWatcher::updateHistory(const std::string &content, int events)
{
    uint64_t now = currentTimeMs();

    AppDbHandler appHistory(APP_HISTORY_PATH, APP_HISTORY_TABLE);
    pid_t pid = std::stoi(content);
    if (events & IN_CREATE) {
        appHistory.insertData(pid, getAppFullPath(pid), now, 0);
    } else {
        appHistory.updateData(pid, now);
    }
}
} // namespace Private
} // namespace Aurora
