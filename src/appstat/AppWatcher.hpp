/*
 * Copyright 2023 Dmitriy Butakov
 */

#pragma once

#include "../common/Watcher.hpp"

#include <cstdint>
#include <map>
#include <string>

namespace Aurora {
namespace Private {
class AppWatcher
{
public:
    explicit AppWatcher();
    ~AppWatcher() = default;

    std::map<std::string, std::map<uint64_t, uint64_t>> getAppActivity() const;

    static void updateHistory(const std::string &, int);

private:
    std::unique_ptr<Watcher> m_procWatcher;
};
} // namespace Private
} // namespace Aurora
