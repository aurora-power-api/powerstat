/*
 * Copyright 2023 Dmitriy Butakov
 */

#pragma once

#include <map>
#include <memory>
#include <string>

#include <sqlite3.h>

#include "../common/Log.hpp"

namespace Aurora {
namespace Private {
class AppDbHandler
{
public:
    explicit AppDbHandler(const std::string &dbPath, const std::string &tableName);
    virtual ~AppDbHandler();

    std::map<std::string, std::map<uint64_t, uint64_t>> getData();
    int insertData(pid_t pid, const std::string &app, uint64_t start, uint64_t end);
    int updateData(pid_t pid, uint64_t end) ;

private:
    enum class Status { Ok, Error };

    sqlite3 *m_db;
    std::string m_dbPath;
    std::string m_tableName;
    Status m_dbStatus;
};
} // namespace Private
} // namespace Aurora
