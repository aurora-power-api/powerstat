/*
 * Copyright 2023 Dmitriy Butakov
 */
#include "AppStat.hpp"

int main()
{
    const char *serviceName = "ru.omp.AppStat";
    auto connection = sdbus::createSystemBusConnection(serviceName);

    const char *objectPath = "/ru/omp/PowerStat";
    Aurora::Private::AppStat service(*connection, objectPath);

    connection->enterEventLoop();
    return 0;
}
