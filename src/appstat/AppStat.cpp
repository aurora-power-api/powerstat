/*
 * Copyright 2023 Dmitriy Butakov
 */

#include "AppStat.hpp"

namespace Aurora {
namespace Private {
AppStat::AppStat(sdbus::IConnection &connection, std::string objectPath)
    : AdaptorInterfaces(connection, std::move(objectPath))
{
    registerAdaptor();
}

AppStat::~AppStat()
{
    unregisterAdaptor();
}

std::vector<sdbus::Struct<std::string, std::vector<sdbus::Struct<uint64_t, uint64_t>>>>
AppStat::getApplicationsActivity()
{
    std::vector<sdbus::Struct<std::string, std::vector<sdbus::Struct<uint64_t, uint64_t>>>>
        appHistory;
    for (const auto &[app, time] : m_appWatcher.getAppActivity()) {
        std::vector<sdbus::Struct<uint64_t, uint64_t>> appActivityTime;
        for (const auto &[start, end] : time) {
            appActivityTime.push_back({start, end});
        }
        appHistory.push_back({app, appActivityTime});
    }
    return appHistory;
}
} // namespace Private
} // namespace  Aurora
