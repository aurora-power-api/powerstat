/*
 * Copyright 2023 Dmitriy Butakov
 */

#include "AppStat_adaptor.hpp"
#include "AppWatcher.hpp"

#include <sdbus-c++/sdbus-c++.h>

namespace Aurora {
namespace Private {
class AppStat : public sdbus::AdaptorInterfaces<ru::omp::PowerApp_adaptor>
{
public:
    AppStat(sdbus::IConnection &connection, std::string objectPath);
    virtual ~AppStat();

protected:
    std::vector<sdbus::Struct<std::string, std::vector<sdbus::Struct<uint64_t, uint64_t>>>>
    getApplicationsActivity() override;

private:
    AppWatcher m_appWatcher;
};

} // namespace Private
} // namespace Aurora
