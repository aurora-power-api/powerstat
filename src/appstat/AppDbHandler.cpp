/*
 * Copyright 2023 Dmitriy Butakov
 */

#include "AppDbHandler.hpp"
#include "../common/Log.hpp"

#include <sqlite3.h>

namespace Aurora {
namespace Private {
AppDbHandler::AppDbHandler(const std::string &dbPath, const std::string &tableName)
    : m_db(nullptr)
    , m_dbPath(dbPath)
    , m_dbStatus(Status::Error)
{
    int rc = sqlite3_open(dbPath.c_str(), &m_db);
    if (rc != SQLITE_OK) {
        LOG_ERROR("Can't open database " << dbPath << ": " << sqlite3_errmsg(m_db));
        sqlite3_close(m_db);
        return;
    }

    char *errMsg = nullptr;
    std::string sql = "CREATE TABLE if not exists " + tableName
                      + "("
                        "APP STRING PRIMARY KEY NOT NULL,"
                        "START INT NOT NULL,"
                        "END INT NOT NULL);";

    rc = sqlite3_exec(m_db, sql.c_str(), nullptr, 0, &errMsg);
    if (rc != SQLITE_OK) {
        LOG_ERROR("SQL error: " << errMsg);
        sqlite3_close(m_db);
        sqlite3_free(errMsg);
        return;
    }

    m_tableName = tableName;
    m_dbStatus = Status::Ok;
}

AppDbHandler::~AppDbHandler()
{
    if (m_db)
        sqlite3_close(m_db);
}

std::map<std::string, std::map<uint64_t, uint64_t>> AppDbHandler::getData()
{
    std::map<std::string, std::map<uint64_t, uint64_t>> result;
    if (m_dbStatus == Status::Error) {
        LOG_ERROR(m_dbPath << " not initiliezed.");
        return result;
    }

    std::string sql = "SELECT APP, START, END from " + m_tableName + ";";
    sqlite3_stmt *stmt = nullptr;
    int rc = sqlite3_prepare_v2(m_db, sql.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("while compiling sql: " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        return result;
    }

    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        // We know, that there are 3 columns for sure
        std::string app = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
        std::string startStr = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1));
        std::string endStr = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 2));
        uint64_t start = std::stoull(startStr);
        uint64_t end = std::stoull(endStr);
        result[app][start] = end;
    }

    if (rc != SQLITE_DONE) {
        LOG_ERROR("select statement didn't finish with DONE" << sqlite3_errmsg(m_db));
    }

    sqlite3_finalize(stmt);

    return result;
}

int AppDbHandler::insertData(pid_t pid, const std::string &app, uint64_t start, uint64_t end)
{
    // TODO: use sqlite DEFAULT TIMESTAMP https://www.sqlite.org/lang_createtable.html#the_default_clause
    // instead of calculating it through C++ code
    if (m_dbStatus == Status::Error) {
        LOG_ERROR(m_dbPath << " not initiliezed.");
        return -1;
    }

    std::string sql = "INSERT INTO " + m_tableName + " (PID, APP, START, END) VALUES (?,?,?,?)";
    sqlite3_stmt *stmt = nullptr;
    int rc = sqlite3_prepare_v2(m_db, sql.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("while compiling sql: " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        return -1;
    }

    std::string pidStr = std::to_string(pid);
    rc = sqlite3_bind_text(stmt, 1, pidStr.c_str(), pidStr.length(), nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("error binding 1st value in insert " << rc << ": " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        sqlite3_close(m_db);
        return -1;
    }

    rc = sqlite3_bind_text(stmt, 2, app.c_str(), app.length(), nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("error binding 2nd value in insert " << rc << ": " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        sqlite3_close(m_db);
        return -1;
    }

    std::string startStr = std::to_string(start);
    rc = sqlite3_bind_text(stmt, 3, startStr.c_str(), startStr.length(), nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("error binding 3rd value in insert " << rc << ": " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        sqlite3_close(m_db);
        return -1;
    }

    std::string endStr = std::to_string(end);
    rc = sqlite3_bind_text(stmt, 4, endStr.c_str(), endStr.length(), nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("error binding 4th value in insert " << rc << ": " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        sqlite3_close(m_db);
        return -1;
    }

    rc = sqlite3_step(stmt);
    sqlite3_finalize(stmt);

    if (rc != SQLITE_DONE) {
        LOG_ERROR("insert statement didn't return DONE: " << rc << ": " << sqlite3_errmsg(m_db));
        return -1;
    }

    return 0;
}

int AppDbHandler::updateData(pid_t pid, uint64_t end) {
    if (m_dbStatus == Status::Error) {
        LOG_ERROR(m_dbPath << " not initiliezed.");
        return -1;
    }

    // Select last app with this pid
    std::string sql = "SELECT APP FROM " + m_tableName + " WHERE PID=? ORDER BY START DESC LIMIT 1;";
    sqlite3_stmt *stmt = nullptr;
    int rc = sqlite3_prepare_v2(m_db, sql.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("while compiling sql: " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        return -1;
    }

    std::string pidStr = std::to_string(pid);
    rc = sqlite3_bind_text(stmt, 1, pidStr.c_str(), pidStr.length(), nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("error binding 1st value in insert " << rc << ": " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        sqlite3_close(m_db);
        return -1;
    }

    rc = sqlite3_step(stmt);
    std::string app(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0)));
    sqlite3_finalize(stmt);

    // Now update time for this PID & App
    sql = "UPDATE " + m_tableName + " WHERE PID=? AND APP=? SET END=?";
    rc = sqlite3_prepare_v2(m_db, sql.c_str(), -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("while compiling sql: " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        return -1;
    }

    rc = sqlite3_bind_text(stmt, 1, pidStr.c_str(), pidStr.length(), nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("error binding 1st value in insert " << rc << ": " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        sqlite3_close(m_db);
        return -1;
    }

    rc = sqlite3_bind_text(stmt, 2, app.c_str(), app.length(), nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("error binding 2nd value in insert " << rc << ": " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        sqlite3_close(m_db);
        return -1;
    }

    std::string endStr = std::to_string(end);
    rc = sqlite3_bind_text(stmt, 3, endStr.c_str(), endStr.length(), nullptr);
    if (rc != SQLITE_OK) {
        LOG_ERROR("error binding 3rd value in insert " << rc << ": " << sqlite3_errmsg(m_db));
        sqlite3_finalize(stmt);
        sqlite3_close(m_db);
        return -1;
    }

    rc = sqlite3_step(stmt);
    sqlite3_finalize(stmt);

    if (rc != SQLITE_DONE) {
        LOG_ERROR("insert statement didn't return DONE: " << rc << ": " << sqlite3_errmsg(m_db));
        return -1;
    }
    return 0;
}

} // namespace Private
} // namespace Aurora